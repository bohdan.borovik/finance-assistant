import { useRef, useState, useContext, useEffect } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Accordion from 'react-bootstrap/Accordion';
import './dates.css';
import OperationsContext from '../../context/OperationsContext';
import ListGroup from 'react-bootstrap/ListGroup';

export default function Dates() {
    const startRef = useRef(null);
    const endRef = useRef(null);
    const [classes, setClasses] = useState('d-none');
    const [invisible, setInvisible] = useState('d-none');
    const [textOnbutton, setTextOnButton] = useState('За весь період');
    const {start, setStart, end, setEnd} = useContext(OperationsContext);
    const [valueStart, setValueStart] = useState('');
    const [valueEnd, setValueEnd] = useState('');
    const [invisibleBlock, setInvisibleBlock] = useState('d-none');

    function chengeTextOnButton(startDateText, endDateText) {
        if (startDateText === endDateText) {
            setTextOnButton(`За ${startDateText}`)
        } else {
        setTextOnButton(startDateText && endDateText ? `Від ${startDateText} до ${endDateText}` : startDateText ? `Від ${startDateText}` : endDateText ? `До ${endDateText}` : `За весь період`)
        }
    }

    useEffect(() => {
        if (start.date || end.date) {
            let startDateText = '';
            let endDateText = '';            
            if (start.date) {let startDate = new Date((start.month + 1) + '-' + start.date + '-' + start.year);
                startDate = startDate.toLocaleDateString();                
                startDateText = startDate.split('.').join('-');
                let reverse = startDateText.split('-').reverse().join('-');
                setValueStart(reverse);}
            if (end.date) {let endDate = new Date((end.month + 1) + '-' + end.date + '-' + end.year);
                endDate = endDate.toLocaleDateString();
                endDateText = endDate.split('.').join('-');
                let reverse = endDateText.split('-').reverse().join('-');
                setValueEnd(reverse);}
            chengeTextOnButton(startDateText, endDateText)
        }
    }, [start, end])

    function initialSort() {
        let start = startRef.current.value;
        let end = endRef.current.value;
        let startDateText, endDateText = '';
        let startDate = [];
        let endDate = [];
        if (start) {startDateText = start.split('-').reverse().join('-');
            start = new Date(start);
            startDate.date = start.getDate();
            startDate.month = start.getMonth();
            startDate.year = start.getFullYear();
        };
        if (end) {endDateText = end.split('-').reverse().join('-');
            end = new Date(end);
            endDate.date = end.getDate();
            endDate.month = end.getMonth();
            endDate.year = end.getFullYear();
        };
        setStart(startDate);
        setEnd(endDate);
        chengeTextOnButton(startDateText, endDateText)
    }

    function toogle() {
        if (classes === 'd-none') {
            setClasses('absolute-dates');
            setInvisible('invisible-box');
        } else {
            setClasses('d-none');
            setInvisible('d-none');
            setInvisibleBlock('d-none');
        }
    }

    function toogleBlock() {
        if (invisibleBlock === 'd-none') {
            setInvisibleBlock('d-block');
        } else {
            setInvisibleBlock('d-none');
        }
    }

    function clearDate() {
        setStart({});
        setEnd({});
        setTextOnButton(`За весь період`);
        setValueStart('');
        setValueEnd('');
        setClasses('d-none');
        setInvisible('d-none');
        setInvisibleBlock('d-none');
    }

    function today() {
        let date = new Date();
        let todayDate = {};
        todayDate.date = date.getDate();
        todayDate.month = date.getMonth();
        todayDate.year = date.getFullYear();
        setStart(todayDate);
        setEnd(todayDate);
    }


    return (<>
        <div className="dates text-center w-100 position-relative">
            <Button id="dropdown" onClick={() => toogle()} variant="dark" >
                {textOnbutton}
            </Button>
            <div className={invisible} onClick={() => toogle()}></div>
            <div className={classes}>
                <ListGroup style={{maxWidth: "25.375rem"}}>
                    <ListGroup.Item>
                        <Button style={{width: "100%"}} variant="outline-dark" onClick={clearDate}>Весь період</Button>
                    </ListGroup.Item>                        
                    <ListGroup.Item>
                        <Button style={{width: "100%"}} variant="outline-dark" onClick={today}>Сьогодні</Button>
                    </ListGroup.Item>
                    <ListGroup.Item>
                        <Button style={{width: "100%"}} variant="outline-dark" onClick={() => toogleBlock()}>Діапазон дат</Button>
                    </ListGroup.Item>                         
                    <ListGroup.Item className={invisibleBlock} >
                        <div className='d-block d-sm-flex justify-content-center m-3'>
                            <Form.Group controlId="formStartDate" className="mb-3 me-sm-3">
                                <Form.Label>Від: </Form.Label>
                                <Form.Control type="date" onChange={initialSort} ref={startRef} defaultValue={valueStart.length ? valueStart : ''}/>
                            </Form.Group>
                            <Form.Group controlId="formEndDate" className="mb-3 ms-sm-3">
                                <Form.Label>До: </Form.Label>
                                <Form.Control type="date" onChange={initialSort} ref={endRef} defaultValue={valueEnd.length ? valueEnd : ''}/>
                            </Form.Group>
                        </div>
                    </ListGroup.Item>                   
                </ListGroup>                
            </div>
        </div>
    </>
    );
}