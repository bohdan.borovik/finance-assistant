import { useContext } from "react";
import { Alert } from "react-bootstrap";
import OperationsContext from "../../context/OperationsContext";

export default function Operation({chekingDate}) {
    
    const {operations, setIdMsg} = useContext(OperationsContext);

    function chek(id) {
        setIdMsg(id);        
    }

    return (
        
        operations.filter(operation => operation.date === chekingDate.date && operation.month === chekingDate.month).map(operation => 
            <Alert key={operation.id} className='d-flex justify-content-between px-2 px-sm-3' variant={operation.class === 'funds' ? 'success': 'danger'} onClick={() => chek(operation.id)} style={{cursor: "pointer"}}>                
                <div className="col-8 col-sm-9 col-md-10">
                    <div className="col-2">{operation.icon}</div>
                    <div className="d-none d-sm-block col-10 col-xxl-10 text-start">
                        <h5><div>{operation.category}</div></h5>
                        <h6><div>{operation.subcategory}</div></h6>
                        <div>{operation.description}</div>
                    </div>
                    <div className="d-block d-sm-none col-10 text-start">
                        <h6><div>{operation.category}</div></h6>
                        <div>{operation.subcategory}</div>
                        <div style={{fontWeight: "300", fontSize: ".8rem"}}>{operation.description}</div>
                    </div>                     
                </div>                
                <h5 className="d-none d-sm-flex align-self-sm-center justify-content-end col-sm-3 col-md-2"><div>{operation.class === 'funds' ? '' : '-'}{operation.value} UAH</div></h5>
                <h6 className="d-flex d-sm-none align-self-sm-center justify-content-end col-4"><div>{operation.class === 'funds' ? '' : '-'}{operation.value} UAH</div></h6>                
            </Alert>
        )
    );
}