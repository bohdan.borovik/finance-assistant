import { useContext, useState } from 'react';
import StorageContext from "../../context/StorageContext";
import OperationsContext from "../../context/OperationsContext";
import SingleOperation from './SingleOperation';
import CreateOperationModal from "./CreateOperationModal";
import ButtonsBlock from './Buttons/ButtonsBlock';
import { Button, Alert } from 'react-bootstrap';

export default function Funds() {

    const {fundsCategories} = useContext(StorageContext);
    const {idMsg, setIdMsg} = useContext(OperationsContext);

    const [showCreateOperation, setShowCreateOperation] = useState(false);
    const [choosenCategory, setChoosenCategory] = useState('')
  
    const hideCreateOperation = () => {setShowCreateOperation(false)};
    const openCreateOperation = (cat) => {setShowCreateOperation(true); setChoosenCategory(cat)};

    return <div className="container fix-main px-0 px-sm-3 py-3 mb-5 mb-lg-0 bg-secondary">
        <div className='d-flex mx-0 mx-lg-3'>
            <div className="d-none d-lg-block col-md-2 mx-md-1 text-center">
                <ButtonsBlock />
            </div>
            <div className="px-0 px-lg-3 col-12 col-lg-10 text-center">       
                <Alert variant="success" className="w-100">
                    <Alert.Heading className='text-end'>Всього на рахунках: {fundsCategories.reduce((acc, category) => acc + category.value, 0)}</Alert.Heading>
                    <hr />
                    {fundsCategories.map((category) => (
                        <Alert key={category.id} className='d-flex justify-content-between px-2 px-sm-3' variant='light' onClick={() => openCreateOperation(category.category)} style={{cursor: "pointer"}}>                
                            <div className="col-8 col-sm-9 col-md-10">
                                <div className="col-2">{category.icon}</div>
                                <div className="d-none d-sm-block col-10 col-xxl-10 text-start">
                                    <h5><div>{category.category}</div></h5>
                                </div>
                                <div className="d-block d-sm-none col-10 text-start">
                                    <h6><div>{category.category}</div></h6>
                                </div>                     
                            </div>                
                            <h5 className="d-none d-sm-flex align-self-sm-center justify-content-end col-sm-3 col-md-2"><div>{category.value} UAH</div></h5>
                            <h6 className="d-flex d-sm-none align-self-sm-center justify-content-end col-4"><div>{category.value} UAH</div></h6>                
                        </Alert>
                    ))}
                    <hr />
                    <div className="d-flex d-lg-none justify-content-between">
                        <ButtonsBlock />
                    </div>
                </Alert>
            </div>
        </div> 
        <SingleOperation />
        <CreateOperationModal showCreate={showCreateOperation} hideCreate={hideCreateOperation} classes={'funds'} category={choosenCategory}/>
    </div>
    ;
}