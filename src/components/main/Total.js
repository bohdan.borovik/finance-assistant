import { useContext, useEffect, useState } from "react";
import { Alert } from "react-bootstrap";
import OperationsContext from "../../context/OperationsContext";

export default function Total() {

    const {operations} = useContext(OperationsContext);
    const [totalFunds, setToltalFunds] = useState(0);
    const [totalOutlays, setToltalOutlays] = useState(0);

    useEffect(() => {
        let allFunds = 0;
        let allOutlays = 0;
        operations.map(operation => operation.class === 'funds' ? allFunds = allFunds + operation.value : allOutlays = allOutlays - operation.value);
        setToltalFunds(allFunds);
        setToltalOutlays(allOutlays);
    }, [operations]);

        return <>
        <div className="text-center w-50">
            <Alert variant="danger" className="m-3"><Alert.Heading className="lh-base mb-1">{totalOutlays}</Alert.Heading></Alert>
        </div>
        <div className="text-center w-50">
            <Alert variant="success" className="m-3"><Alert.Heading className="lh-base mb-1">{totalFunds}</Alert.Heading></Alert>
        </div>
        </>;
}