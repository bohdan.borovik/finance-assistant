import { useRef, useContext, useEffect, useState } from "react";
import { Form, FloatingLabel, Button } from "react-bootstrap";
import StorageContext from "../../../context/StorageContext";
import Modal from 'react-bootstrap/Modal';
import AddCircleIcon from '@mui/icons-material/AddCircle';

export default function CreateNewClass({showCreate, hideCreate, loadClass}) {
    const {fundsCategories, setFundsCategories, outlaysCategories, setOutlaysCategories} = useContext(StorageContext);
    const [classBuffer, setClassBuffer] = useState([]);
    const [isDisabledOnClick, setIsDisabledOnClick] = useState(false);
    const [isDisabled, setIsDisabled] = useState(false);
    const categoryRef = useRef(null);

    useEffect(() => {
        if (loadClass === 'funds') {setClassBuffer(fundsCategories)}
        else {setClassBuffer(outlaysCategories)}
    }, [])

    function createNewClass() {
        if (categoryRef.current.value === '') {setIsDisabled(true); return}
        else {setIsDisabled(false)}
        let buffer = classBuffer.slice(0);      
        let newId = buffer.length + 1;
        buffer.push(loadClass === 'funds' ? {id: newId,
            icon: '', 
            category: categoryRef.current.value,
            value: 0
        } : {id: newId,
            icon: '', 
            category: categoryRef.current.value
        });
        
        if (loadClass === 'funds') {
            localStorage.setItem('FundsCategories', JSON.stringify(buffer)); 
            setFundsCategories(buffer);
        } else {
            localStorage.setItem('OutlaysCategories', JSON.stringify(buffer));
            setOutlaysCategories(buffer);
        }
        
        hideCreate();
    };

    function checkClass(e) {
        let buffer = classBuffer.filter(el => el.category === e.target.value);
        if (buffer.length) {setIsDisabledOnClick(true)}
        else {setIsDisabledOnClick(false); setIsDisabled(false)}
    }

    function close() {
        setIsDisabled(false);
        hideCreate();
        setIsDisabledOnClick(false); 
        setIsDisabled(false);
    }

    return <><Modal show={showCreate} onHide={hideCreate} backdrop="static" centered>
        <Modal.Header>
            <Modal.Title>{loadClass === 'funds' ? `Створити новий рахунок` : `Створити нову категорію`}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form className='my-2 align-items-center'>
                <FloatingLabel  className='my-2' controlId="floatingCategiry" label={loadClass === 'funds' ? `Рахунок` : `Категорію`}>                
                    <Form.Control  ref={categoryRef}
                                type="text" 
                                placeholder={loadClass === 'funds' ? `Новий рахунок` : `Нова категорія`} 
                                onKeyUp={(e) => checkClass(e)}
                                required isInvalid={isDisabled || isDisabledOnClick}/>
                    <Form.Control.Feedback type="invalid">
                        {isDisabledOnClick ? 'Категорія існує' : 'Категорія не вказана'}
                    </Form.Control.Feedback>                                
                </FloatingLabel>
            </Form>
            <hr />
            <div className='d-flex justify-content-between'>
                <Button variant="secondary" onClick={close}>
                    Скасувати
                </Button>
                <Button variant="primary" onClick={createNewClass} disabled={isDisabled || isDisabledOnClick}>
                    Створити
                </Button>
            </div>
        </Modal.Body>
    </Modal></>
}