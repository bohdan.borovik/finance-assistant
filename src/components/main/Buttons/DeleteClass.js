import { useRef, useContext, useEffect, useState } from "react";
import { Form, Button } from "react-bootstrap";
import StorageContext from "../../../context/StorageContext";
import Modal from 'react-bootstrap/Modal';

export default function DeleteClass({showDelete, hideDelete, loadClass}) {
    const {fundsCategories, setFundsCategories, outlaysCategories, setOutlaysCategories, storage, setStorage} = useContext(StorageContext);
    const [classBuffer, setClassBuffer] = useState([]);
    const categoryRef = useRef(null);
    const [isDisabled, setIsDisabled] = useState(false);

    useEffect(() => {
        if (loadClass === 'funds') {setClassBuffer(fundsCategories)}
        else {setClassBuffer(outlaysCategories)}
    }, [outlaysCategories, fundsCategories]);

    function deleteOperations() {
        let buffer = storage.filter(el => el.category === categoryRef.current.value || el.fund === categoryRef.current.value);
        let storageBuffer = storage.slice(0);
        buffer.forEach(el => storageBuffer.splice(storageBuffer.indexOf(el), 1));        
        if (loadClass !== 'funds') {
        let fundsBuffer = fundsCategories.map(fund => ({...fund, value: fund.value + buffer.reduce((acc, el) => fund.category === el.fund ? acc + el.value: acc + 0, 0)})); 
        setFundsCategories(fundsBuffer);}
        setStorage(storageBuffer);
        hideDelete()
    }

    function deleteCategory() {
        let buffer = classBuffer.slice(0);
        let bufferArr = buffer.filter(el => el.category === categoryRef.current.value);
        buffer.splice(buffer.indexOf(bufferArr[0]), 1);
        if (loadClass === 'funds') {
            localStorage.setItem('FundsCategories', JSON.stringify(buffer)); 
            setFundsCategories(buffer);
        } else {
            localStorage.setItem('OutlaysCategories', JSON.stringify(buffer)); 
            setOutlaysCategories(buffer);
        }
        deleteOperations()
    };

    function checkSudmit() {
        if (!categoryRef.current.value) {setIsDisabled(true); return}
        let chek = window.confirm(`Видалити ${loadClass === 'funds' ? `рахунок` : `категорію`} "${categoryRef.current.value}"? При підтвердженні разом з ${loadClass === 'funds' ? `ним` : `нею`}  видаляться всі пов'язані операції`);
        if (chek) {deleteCategory()}
        else {hideDelete()}
    }

    function close() {
        setIsDisabled(false);
        hideDelete();
    }

    return <><Modal show={showDelete} onHide={hideDelete} backdrop="static" centered>
        <Modal.Header>
            <Modal.Title>{loadClass === 'funds' ? `Видалити рахунок` : `Видалити операцію`}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form className='my-2 align-items-center'>
                <Form.Select ref={categoryRef} 
                    className='mb-2' 
                    aria-label="Категорія" 
                    onChange={() => setIsDisabled(false)} 
                    required 
                    isInvalid={isDisabled}>
                <option key='heading-string' value=''>{loadClass === 'funds' ? `Рахунок` : `Категорія`}</option>
                {classBuffer.map(el => <option key={el.category} value={el.category}>{el.category}</option>)}
                </Form.Select>
                <Form.Control.Feedback type="invalid">
                    Категорія не вказана
                </Form.Control.Feedback>
            </Form>
            <hr />
            <div className='d-flex justify-content-between'>
                <Button variant="secondary" onClick={close}>
                    Скасувати
                </Button>
                <Button variant="primary" onClick={checkSudmit} disabled={isDisabled}>
                    Видалити
                </Button>
            </div>
        </Modal.Body>
    </Modal></>
}