import { useState } from "react";
import { Button } from "react-bootstrap";
import CreateNewClass from "./CreateNewClass";
import { SvgIcon } from "@mui/material";
import AddCircleIcon from '@mui/icons-material/AddCircle';

export default function ButtonCreateClass({loadClass}) {

    const [showCreate, setShowCreate] = useState(false);
  
    const hideCreate = () => {setShowCreate(false)};
    const openCreate = () => {setShowCreate(true)};

    return <><Button key={`${loadClass}_1`} variant="dark" onClick={openCreate} className="d-none d-lg-block mb-2 w-100">Створити</Button>
        <Button key={`${loadClass}_2`} variant="dark" onClick={openCreate} className="d-block d-lg-none block-btn-rounded"><SvgIcon component={AddCircleIcon} style={{fontSize: "2rem"}}/></Button>
        <CreateNewClass showCreate={showCreate} hideCreate={hideCreate} loadClass={loadClass}/></>;
}