import { useState } from "react";
import { Button } from "react-bootstrap";
import { SvgIcon } from "@mui/material";
import AddCircleIcon from '@mui/icons-material/AddCircle';
import CreateNewPlan from "../CreateNewPlan";

export default function ButtonCreatePlan({loadClass}) {

    const [showCreate, setShowCreate] = useState(false);
  
    const hideCreate = () => {setShowCreate(false)};
    const openCreate = () => {setShowCreate(true)};

    return <><Button key={`${loadClass}_1`} variant="dark" onClick={openCreate} className="d-none d-lg-block mb-2 w-100">Створити</Button>
        <Button key={`${loadClass}_2`} variant="dark" onClick={openCreate} className="d-block d-lg-none block-btn-rounded"><SvgIcon component={AddCircleIcon} style={{fontSize: "2rem"}}/></Button>
        <CreateNewPlan showCreate={showCreate} hideCreate={hideCreate} loadClass={loadClass}/></>;
}