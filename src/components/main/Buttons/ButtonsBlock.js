import { Button } from "react-bootstrap";
import BtnCreateOperation from "../BtnCreateOperation";
import { useState, useEffect } from "react";
import { useLocation } from 'react-router-dom';
import ControlButtons from "./ControlButtons";
import './buttonsBlock.css'


export default function ButtonsBlock() {
    
    const [activeElement, setActiveElement] = useState(0);

    const buttons = [
        { value: 1, path: 'funds'},
        { value: 2, path: 'categories'},
        { value: 3, path: 'plans'}
    ];

    const location = useLocation();

    useEffect(() => {
        let path = location.pathname;
        path = path.slice(1,path.length);
        buttons.forEach(button => button.path === path ? setActiveElement(button.value) : 0);
    }, []);

    return <>
    {activeElement ? 
        buttons.map((el, indx) => el.value === activeElement ? <>
            <ControlButtons key={`${el.path}_ControlButtons_${indx}`} loadClass={el.path}/>
         </> : '') : <>
            <BtnCreateOperation key={activeElement + 1} />
            </>
    }
    </>;
}