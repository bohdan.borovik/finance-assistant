import ButtonCreateClass from "./ButtonCreateClass";
import ButtonDeleteClass from "./ButtonDeleteClass";
import ButtonCreatePlan from "./ButtonCreatePlan";
import ButtonDeletePlan from "./ButtonDeletePlan"

export default function ControlButtons({loadClass}) {

    
    return loadClass === 'plans' ? <>
    <ButtonCreatePlan key={`${loadClass}_button_1`} loadClass={loadClass}/>
    <ButtonDeletePlan key={`${loadClass}_button_2`} loadClass={loadClass}/>
    </> : <>
        <ButtonCreateClass key={`${loadClass}_button_1`} loadClass={loadClass}/>
        <ButtonDeleteClass key={`${loadClass}_button_2`} loadClass={loadClass}/>
    </>;
}