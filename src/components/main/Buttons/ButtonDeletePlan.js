import { useState } from "react";
import { Button } from "react-bootstrap";
import { SvgIcon } from "@mui/material";
import DeleteForeverIcon from '@mui/icons-material/DeleteForever';
import DeletePlan from "../DeletePlan";

export default function ButtonDeletePlan({loadClass}) {

    const [showDelete, setShowDelete] = useState(false);
  
    const hideDelete = () => {setShowDelete(false)};
    const openDelete = () => {setShowDelete(true)};

    return <><Button key={`${loadClass}_3`} variant="dark" onClick={openDelete} className="d-none d-lg-block mt-2 w-100">Видалити</Button>
    <Button key={`${loadClass}_4`} variant="dark" onClick={openDelete} className="d-block d-lg-none block-btn-rounded"><SvgIcon component={DeleteForeverIcon} style={{fontSize: "2rem"}}/></Button>
    <DeletePlan showDelete={showDelete} hideDelete={hideDelete} loadClass={loadClass}/></>;
}