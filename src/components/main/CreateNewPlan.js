
import { useRef, useContext, useState } from "react";
import { Form, FloatingLabel, Button } from "react-bootstrap";
import Modal from 'react-bootstrap/Modal';
import StorageContext from "../../context/StorageContext";

export default function CreateNewPlan({showCreate, hideCreate, loadClass}) {
    const {allPlans, setAllPlans} = useContext(StorageContext);
    const [isDisabledDate, setIsDisabledDate] = useState(false);
    const [isDisabledName, setIsDisabledName] = useState(false);
    const [isDisabledValue, setIsDisabledValue] = useState(false);
    const [isDisabledNumber, setIsDisabledNumber] = useState(false);
    const dateRef = useRef(null);
    const nameRef = useRef(null);
    const descriptionRef = useRef(null);
    const valueRef = useRef(null);
    const chekRef = useRef(null);
    function createNewPlan() {
        let buffer = allPlans.slice(0);      
        let newId = buffer.length + 1;
        let inputDate = dateRef.current.value;
        let dateArr = {};
        if (inputDate) {
            inputDate = new Date(inputDate);
            dateArr.date = inputDate.getDate();
            dateArr.month = inputDate.getMonth();
            dateArr.year = inputDate.getFullYear();
        }
        buffer.push({id: newId,
            name: nameRef.current.value,
            description: descriptionRef.current.value,
            date: dateArr.date,
            month: dateArr.month,
            year: dateArr.year,
            repeat: chekRef.current.value,
            value: valueRef.current.value
        });
        localStorage.setItem('Plans', JSON.stringify(buffer));
        setAllPlans(buffer);
        close();
    };

    function checkNumber(e) {
        if (Number(e.target.value)) {setIsDisabledNumber(false);setIsDisabledValue(false)}
        else {setIsDisabledNumber(true)}
    }

    function checkDate(e) {
        if (e.target.value) {setIsDisabledDate(false)}
        else {setIsDisabledDate(true)}
    }

    function checkName(e) {
        if (e.target.value) {setIsDisabledName(false)}
        else {setIsDisabledName(true)}
    }

    function checkForm() {
        let date, name, value = false;
        if (!dateRef.current.value) {date = true; setIsDisabledDate(true)}
        if (!nameRef.current.value) {name = true; setIsDisabledName(true)}
        if (!valueRef.current.value) {value = true; setIsDisabledValue(true)}
        if (date || name || value) {return}
        else {createNewPlan()}
    }

    function close() {
        setIsDisabledName(false);
        setIsDisabledDate(false);
        setIsDisabledNumber(false);setIsDisabledValue(false);
        hideCreate();
    }

    return <><Modal show={showCreate} onHide={hideCreate} backdrop="static" centered>
        <Modal.Header>
            <Modal.Title>Створити новий план</Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form className='my-2 align-items-center'>
                <FloatingLabel className='mb-2' controlId="floatingDate" label="Дата виконання">
                    <Form.Control ref={dateRef} 
                                onChange={(e) => checkDate(e)}
                                type="date" 
                                placeholder="Дата виконання"
                                required isInvalid={isDisabledDate}/>
                    </FloatingLabel>
                    <Form.Control.Feedback type="invalid">
                        Вкажіть назву
                    </Form.Control.Feedback>
                <FloatingLabel  className='my-2' controlId="floatingName" label="Назва">                
                    <Form.Control  ref={nameRef}
                                onChange={(e) => checkName(e)}
                                type="text" 
                                placeholder="Назва"
                                required isInvalid={isDisabledName}/>
                    <Form.Control.Feedback type="invalid">
                        Вкажіть назву
                    </Form.Control.Feedback>                                
                </FloatingLabel>
                <FloatingLabel  className='my-2' controlId="floatingDescription" label="Опис">                
                    <Form.Control  ref={descriptionRef}
                                type="text" 
                                placeholder="Опис"/>                              
                </FloatingLabel>
                <FloatingLabel  className='my-2' controlId="floatingValue" label="Сума">                
                    <Form.Control  ref={valueRef}
                                type="text" 
                                placeholder="Сума"
                                onKeyUp={(e) => checkNumber(e)}
                                required isInvalid={isDisabledValue || isDisabledNumber}/>
                    <Form.Control.Feedback type="invalid">
                        {isDisabledNumber ? 'Тільки числа': 'Вкажіть сумму'}
                    </Form.Control.Feedback>                                
                </FloatingLabel>
                <Form.Check
                    inline
                    label="Повторювати щомісяця"
                    name="group1"
                    type='checkbox'
                    id='checkbox'
                    ref={chekRef}/>
            </Form>
            <hr />
            <div className='d-flex justify-content-between'>
                <Button variant="secondary" onClick={close}>
                    Скасувати
                </Button>
                <Button variant="primary" onClick={checkForm} disabled={isDisabledValue || isDisabledName || isDisabledDate || isDisabledNumber}>
                    Створити
                </Button>
            </div>
        </Modal.Body>
    </Modal></>
}