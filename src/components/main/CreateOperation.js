import { useRef, useContext, useState, useEffect} from 'react';
import { Alert, Form, FloatingLabel, Button } from 'react-bootstrap';
import StorageContext from '../../context/StorageContext';
import OperationsContext from '../../context/OperationsContext';

export default function CreateOperation({hideCreate, operationClass, category, plan, planDone}) {
    
    const dateRef = useRef(null);
    const classRef = operationClass;
    const iconRef = useRef(null);
    const categoryRef = useRef(null);
    const subcategoryRef = useRef(null);
    const valueRef = useRef(null);
    const fundRef = useRef(null);
    const descriptionRef = useRef(null);
    const {storage, setStorage, fundsCategories, setFundsCategories, outlaysCategories} = useContext(StorageContext);
    const [checkOperation, setCheckOperation] = useState([]);
    const {start, setStart, end, setEnd} = useContext(OperationsContext);
    const [isDisabled, setIsDisabled] = useState(false);
    
    const [dateNow, setDateNow] = useState('');

    useEffect(() => {
        let now = new Date();
        now = now.toLocaleDateString();
        now = now.split('.').reverse().join('-');
        setDateNow(now);
        if (operationClass === 'funds') {
            setCheckOperation(fundsCategories)
        } else {
            setCheckOperation(outlaysCategories)
        }
    }, []);

    function pay() {
        let payment = Number(valueRef.current.value);
        let buffer = [];
        if (operationClass !== 'funds') {
            buffer = fundsCategories.map(el => ({...el, value: el.category === fundRef.current.value ? el.value - payment : el.value}));
        } else {
            buffer = fundsCategories.map(el => ({...el, value: el.category === categoryRef.current.value || category ? el.value + payment : el.value}));
        }
        localStorage.setItem('FundsCategories', JSON.stringify(buffer));
        setFundsCategories(buffer);
    }

    function addOperation() {       
        if (valueRef.current.value === 0 || categoryRef.current.value === 0) {return}
        let buffer = storage.slice(0);
        let newId = buffer.length + 1;
        let inputDate = dateRef.current.value;
        let dateArr = {};
        if (inputDate) {
            inputDate = new Date(inputDate);
            dateArr.date = inputDate.getDate();
            dateArr.month = inputDate.getMonth();
            dateArr.year = inputDate.getFullYear();
        }
        buffer.push(operationClass === 'funds' ? {id: newId,
            class: classRef,
            icon: '', 
            category: category ? category : categoryRef.current.value, 
            subcategory: subcategoryRef.current.value, 
            value: Number(valueRef.current.value),
            date: dateArr.date,
            month: dateArr.month,
            year: dateArr.year,
            description: descriptionRef.current.value,
        } : {id: newId,
            class: classRef,
            icon: '', 
            category: category ? category : categoryRef.current.value,
            fund: fundRef.current.value, 
            subcategory: subcategoryRef.current.value, 
            value: Number(valueRef.current.value),
            date: dateArr.date,
            month: dateArr.month,
            year: dateArr.year,
            description: descriptionRef.current.value,
        });
        setStorage(buffer);
        localStorage.setItem('Storage', JSON.stringify(buffer));
        setStart(dateArr);
        setEnd(dateArr);
        pay();
        if (plan) {planDone()}
        hideCreate();
    }

    function checkNumber(e) {
        if (Number(e.target.value)) {setIsDisabled(false)}
        else {setIsDisabled(true)}
    }

    return (
        <><Alert variant={operationClass === 'funds' ? "success" : "danger"}>
        <Form className='my-4 align-items-center text-dark'>
            <FloatingLabel className='mb-2' controlId="floatingDate" label="Дата">
                <Form.Control ref={dateRef} 
                            type="date" 
                            placeholder="Дата" defaultValue={dateNow}/>
            </FloatingLabel>
            {!category ? <Form.Select ref={categoryRef} className='mb-2 py-3' aria-label="Категорія">
                <option>Категорія</option>
                {checkOperation.map(el => <option key={el.category} value={el.category}>{el.category}</option>)}
            </Form.Select> :
            <FloatingLabel ref={categoryRef} className='mb-2' controlId="floatingCategiry" label="Категорія">
                <Form.Control  
                            type="text" 
                            placeholder="Категорія" defaultValue={category ? category : ''}  disabled/>
            </FloatingLabel>}
            <FloatingLabel className='mb-2' controlId="floatingSubcategiry" label="Коментар">
                <Form.Control ref={subcategoryRef} 
                            type="text" 
                            placeholder="Кометнар" />
            </FloatingLabel>
            <FloatingLabel className='mb-2' controlId="floatingValue" label="Сума">
                <Form.Control ref={valueRef} 
                            type="text" 
                            placeholder="Сума" 
                            onKeyUp={(e) => checkNumber(e)}
                            required isInvalid={isDisabled}/>
                <Form.Control.Feedback type="invalid">
                    Тільки числа
                </Form.Control.Feedback>
            </FloatingLabel>
            {operationClass !== 'funds' ? <Form.Select ref={fundRef} className='mb-2 py-3' aria-label="З рахунку">
                <option>Рахунок</option>
                {fundsCategories.map(el => <option key={el.category} value={el.category}>{el.category}</option>)}
            </Form.Select> : ''}
            <FloatingLabel className='mb-2' controlId="floatingDescription" label="Опис">
                <Form.Control ref={descriptionRef} 
                            type="text" 
                            placeholder="Опис" />
            </FloatingLabel>
        </Form>
        </Alert>
        <div className='d-flex justify-content-between mx-3 mb-3'>
            <Button variant="secondary" onClick={hideCreate}>
                Скасувати
            </Button>
            <Button variant="primary" onClick={() => addOperation()} disabled={isDisabled}>
                Створити
            </Button>
        </div>
        </>
    );
}