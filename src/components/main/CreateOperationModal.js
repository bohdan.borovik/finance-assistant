import Modal from 'react-bootstrap/Modal';

import { Tab, Tabs } from 'react-bootstrap';
import CreateOperation from './CreateOperation';

export default function CreateOperationModal({showCreate, hideCreate, classes, category, plan, planDone}) {

    return (
        <>      
        <Modal show={showCreate} onHide={hideCreate} backdrop="static" centered>
            <Modal.Header closeButton>
                <Modal.Title>Створити нову операцію</Modal.Title>
            </Modal.Header>
            <Modal.Body className='p-0'>
                {classes.length === 2 ? <Tabs
                defaultActiveKey="outlay"
                transition={false}
                id="noanim-tab-example"
                className="mb-3">
                    <Tab eventKey="fund" title="Поповнення">
                        <CreateOperation hideCreate={hideCreate} operationClass={'funds'} plan={plan} planDone={planDone}/>
                    </Tab>
                    <Tab eventKey="outlay" title="Витрата">
                        <CreateOperation hideCreate={hideCreate} operationClass={'outlays'} plan={plan} planDone={planDone}/>
                    </Tab>
                </Tabs> : <CreateOperation hideCreate={hideCreate} operationClass={classes} category={category} />}           
            </Modal.Body>
        </Modal>
        </>
    );
}