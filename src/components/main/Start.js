import { useContext, useRef, useState, useEffect } from 'react';
import ProfileContext from '../../context/ProfileContext';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Card from 'react-bootstrap/Card';
import { FloatingLabel, ButtonGroup } from 'react-bootstrap';
import './main.css'
export default function Start() {

    const {user, setUser} = useContext(ProfileContext);
    const loginRef = useRef(null);
    const passwordRef = useRef(null);

    const [isLogin, setIsLogin] = useState(false);
    const [isPassword, setIsPassword] = useState(false);
    const [incorrect, setIncorrect] = useState(false);
    const [isDisable, setIsDisable] = useState(false);

    const [storageData, setStorageData] = useState({});

    useEffect(() => {
        setStorageData(JSON.parse(localStorage.getItem('Profile')));
    } , []);
    

    function logInProfile() {
        localStorage.setItem('Profile',JSON.stringify({login: loginRef.current.value, password: passwordRef.current.value, isLogIn: true}));
        setUser({login: loginRef.current.value, password: passwordRef.current.value, isLogIn: true});   
    }

    function checkPass(e) {
        if (e.target.value) {setIsPassword(false); setIncorrect(false); setIsLogin(false);}
    }

    function checkLog(e) {
        if (e.target.value) {setIsLogin(false); setIsDisable(false); setIncorrect(false)}
    }

    function checkLogin() {
        let pass, log = false;
        setIsDisable(false);
        if (!loginRef.current.value) {log = true; setIsLogin(true)}
        if (!passwordRef.current.value) {pass = true; setIsPassword(true)}
        if (storageData.login !== loginRef.current.value || storageData.password !== passwordRef.current.value) {
            setIsLogin(true); 
            setIsPassword(true); 
            setIncorrect(true);
            log = true;
            pass = true;}
        if (pass || log) {return;}
        
        else {logInProfile()}
    }

    function checkSignIn() {
        let pass, log = false;
        if (!loginRef.current.value) {log = true; setIsLogin(true)}
        if (!passwordRef.current.value) {pass = true; setIsPassword(true)}
        if (storageData.login === loginRef.current.value) {log = true; setIsLogin(true); setIsDisable(true)}
        if (pass || log) {return}
        else {logInProfile()}
    }

    return <div className="container  px-0 px-sm-3 py-3 mb-0 mb-lg-0 bg-secondary">
        <div className='w-100 h-100 fix-start d-flex align-items-center justify-content-center'>
            <Card style={{ width: '18rem' }}>
                <Card.Body>        
                    <Card.Text className='text-center'>
                    Увійдіть або зареєструйтесь, щоб розпочати роботу
                    </Card.Text>
                    <Form>                        
                        <Form.Group className="mb-3" controlId="formLogin">
                            <FloatingLabel className='mb-2' controlId="floatingLogin" label="Логін">
                                <Form.Control type="text" 
                                onChange={(e) => checkLog(e)}
                                placeholder='Логін' 
                                ref={loginRef}
                                required isInvalid={isLogin}/>
                                <Form.Control.Feedback type="invalid">                                    
                                    {isDisable ? "Такий користувач вже зареєстрований" : incorrect ? "" : "Введіть логін"}
                                </Form.Control.Feedback>
                            </FloatingLabel>                            
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formPassword">
                            <FloatingLabel className='mb-2' controlId="floatingPassword" label="Пароль">
                                <Form.Control type="password" 
                                onChange={(e) => checkPass(e)}
                                placeholder='Пароль' 
                                ref={passwordRef}
                                required isInvalid={isPassword}/>
                                <Form.Control.Feedback type="invalid">
                                    {incorrect ? "Невірний логін чи пароль" : "Введіть пароль"}
                                </Form.Control.Feedback>
                            </FloatingLabel>                            
                        </Form.Group>
                        <div className='text-center'>
                        <ButtonGroup aria-label="checkInButtons">
                            <Button variant="dark" onClick={checkSignIn} disabled={isDisable}>
                                Реєстрація
                            </Button>
                            <Button variant="dark" onClick={checkLogin}>
                                Увійти
                            </Button>
                        </ButtonGroup>
                        </div>
                        
                    </Form>
                </Card.Body>
            </Card>   
        </div>
         
    </div>;
}