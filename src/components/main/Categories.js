import { useContext, useEffect, useState } from 'react';
import StorageContext from "../../context/StorageContext";
import OperationsContext from "../../context/OperationsContext";
import SingleOperation from './SingleOperation';
import CreateOperationModal from "./CreateOperationModal";
import Dates from './Dates';
import { Alert } from 'react-bootstrap';
import ProgressBar from 'react-bootstrap/ProgressBar';
import ButtonsBlock from './Buttons/ButtonsBlock';



export default function Categories() {

    const {outlaysCategories} = useContext(StorageContext);
    const {idMsg, setIdMsg, operations} = useContext(OperationsContext);
    const [totalByCategories, setTotalByCategories] = useState([]);

    const [showCreateOperation, setShowCreateOperation] = useState(false);
    const [choosenCategory, setChoosenCategory] = useState('')
  
    const hideCreateOperation = () => {setShowCreateOperation(false)};
    const openCreateOperation = (cat) => {setShowCreateOperation(true); setChoosenCategory(cat)};

    useEffect(() => {
        let buffer = [];
        outlaysCategories.map(el => buffer.push(operations.reduce((acc, operation) => operation.category === el.category ? acc + operation.value : acc + 0, 0)));
        setTotalByCategories(buffer);
    }, [operations, outlaysCategories])

    const [total, setTotal] = useState(0);

    useEffect(() => {
        setTotal(totalByCategories.reduce((acc, category) => acc + category, 0));
    }, [totalByCategories])

    const {setOperations, start, setStart, end, setEnd} = useContext(OperationsContext);
    const {storage} = useContext(StorageContext);

    useEffect(() => {
        if (start.date && end.date) {setOperations(storage.filter((operation) => operation.month >= start.month && operation.month <= end.month && operation.date >= start.date && operation.date <= end.date))}
        else if (start.date) {setOperations(storage.filter((operation) => (operation.month === start.month && operation.date >= start.date) ||  operation.month > start.month))}
        else if (end.date) {setOperations(storage.filter((operation) => (operation.month === end.month && operation.date <= end.date) || operation.month < end.month))}
        else {setOperations(storage)};
    }, [start, end])

    return <div className="container fix-main px-0 px-sm-3 py-3 mb-5 mb-lg-0 bg-secondary ">
        <Dates />
        <div className='d-flex my-3 mx-0 mx-lg-3'>
            <div className="d-none d-lg-block col-md-2 mx-md-1 text-center">
                <ButtonsBlock />
            </div>
            <div className="px-0 px-lg-3 col-12 col-lg-10 text-center">     
                <Alert variant="danger" className='w-100'>
                    <Alert.Heading className='text-end'>Всього витрачено: {total}</Alert.Heading>
                    <hr />
                    {outlaysCategories.map((category, indx) => {if (start.date || end.date) {if (totalByCategories[indx]>0) {return category}} else {return category}})
                    .map((category, indx) => { return <> {category ?
                        <Alert key={category.id} variant='light' onClick={() => openCreateOperation(category.category)} style={{cursor: "pointer"}}>                
                            <div className='d-flex justify-content-between' >
                                <div className="col-8 col-sm-9 col-md-10 text-start">
                                    <div className="col-2">{category.icon}</div>
                                    <div className="d-none d-sm-block col-10 col-xxl-10">
                                        <h5><div>{category.category}</div></h5>
                                    </div>
                                    <div className="d-block d-sm-none col-10">
                                        <h6><div>{category.category}</div></h6>
                                    </div>                     
                                </div>                
                                <h5 className="d-none d-sm-block text-end col-sm-3 col-md-2"><div>{totalByCategories[indx]} UAH</div></h5>
                                <h6 className="d-block d-sm-none text-end col-4"><div>{totalByCategories[indx]} UAH</div></h6>
                            </div>
                            <div>
                                <div></div>
                                <div className='text-center'>{`${(totalByCategories[indx]/total*100).toFixed(2)}%`}</div>
                                <ProgressBar variant='danger' now={totalByCategories[indx]/total*100}/>
                            </div>                
                        </Alert> : ""}                        
                        </>})}
                        {!total ? <Alert>За цей період немає жодних витрат</Alert> : ''}
                    <hr />
                    <div className="d-flex d-lg-none justify-content-between">
                        <ButtonsBlock />
                    </div>
                </Alert>
                
            </div>
        </div>
        <SingleOperation />
        <CreateOperationModal showCreate={showCreateOperation} hideCreate={hideCreateOperation} classes={'outlays'} category={choosenCategory}/>
    </div>
    ;
}