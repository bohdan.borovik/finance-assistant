import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function NotLogIn() {
    return <div className="container  px-0 px-sm-3 py-3 mb-0 mb-lg-0 bg-secondary">
    <div className='w-100 fix-start d-flex flex-column align-items-center justify-content-center'>
        <div className="text-center">
        Для роботи потрібно увійти або зареєструватися
        </div>
        <Link to="/"><Button variant="dark">Увійти / зареєструватися</Button></Link>   
    </div>
     
</div>;;
}