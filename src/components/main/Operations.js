import { useContext, useEffect } from 'react';
import OperationsContext from '../../context/OperationsContext';
import StorageContext from "../../context/StorageContext";
import Dates from './Dates';
import Total from './Total';
import './main.css'
import SingleOperation from './SingleOperation';
import OperationsList from './OperationsList';
import ButtonsBlock from './Buttons/ButtonsBlock';

export default function Operations() {

    const {setOperations, start, end} = useContext(OperationsContext);
    const {storage} = useContext(StorageContext);

   useEffect(() => {
            if (start.date && end.date) {setOperations(storage.filter((operation) => operation.month >= start.month && operation.month <= end.month && operation.date >= start.date && operation.date <= end.date))}
            else if (start.date) {setOperations(storage.filter((operation) => (operation.month === start.month && operation.date >= start.date) ||  operation.month > start.month))}
            else if (end.date) {setOperations(storage.filter((operation) => (operation.month === end.month && operation.date <= end.date) || operation.month < end.month))}
            else {setOperations(storage)};
        }, [start, end])

    return (<div className="container fix-main px-0 px-sm-3 py-3 mb-5 mb-lg-0 bg-secondary">        
        <div>
            <Dates />
            <div className='d-flex'>
                <Total />
            </div>
            <div className='d-flex mx-0 mx-lg-3'>
                <div className="d-none d-lg-block col-lg-2 mx-lg-1 text-center my-3">
                    <ButtonsBlock />
                </div>
                <OperationsList />
            </div>            
        </div>
        <SingleOperation />
        <div className="d-block d-lg-none">
            <ButtonsBlock />
        </div>
    </div>)
    ;
}