import { useState } from "react";
import { Button } from "react-bootstrap";
import CreateOperationModal from "./CreateOperationModal";
import './createOperation.css';
import { SvgIcon } from "@mui/material";
import AddCircleIcon from '@mui/icons-material/AddCircle';

export default function BtnCreateOperation() {
    const [showCreate, setShowCreate] = useState(false);
    const [test, setTest] = useState('Button');
  
    const hideCreate = () => {setShowCreate(false)};
    const openCreate = () => {setShowCreate(true)};
    
    return <>
        <div className="d-block d-lg-none btn-create"><SvgIcon variant="dark"  onClick={openCreate} component={AddCircleIcon} style={{fontSize: "3.5rem", margin: "-.5rem"}} /></div>
        <Button variant="dark" className="d-none d-lg-block w-100" onClick={openCreate}>Нова операція</Button>
          
    <CreateOperationModal showCreate={showCreate} hideCreate={hideCreate} classes={['funds', 'outlays']}/>
    </>;
}