import { useContext, useEffect, useState } from "react";
import OperationsContext from "../../context/OperationsContext";
import { Alert } from "react-bootstrap";
import Operation from "./Operation";

export default function OperationsList() {
    const {operations} = useContext(OperationsContext);
    
    const [chekingDates, setChekingDates] = useState([]);
    const [nameMonth, setNameMonth] = useState([]);

    useEffect(() => {
        let years = operations.reduce((acc, operation) => {
            if (!acc.includes(operation.year)) 
            {acc.push(operation.year)}; 
        return acc}, []);

        let checked = [];

        years.map(operationYear => {
            operations.filter(operation => operation.year === operationYear)
            .reduce((acc, operation) => {
                if (!acc.includes(operation.month)) 
                {acc.push(operation.month)}; 
            return acc}, []).map(operationMonth => {
                return operations.filter(operation => operation.month === operationMonth)
                .reduce((acc, operation) => {if (!acc.includes(operation.date)) {acc.push(operation.date)}; return acc}, [])
                .map(operationDate => checked.push({date: operationDate, month: operationMonth, year: operationYear}))
            })
        });
        checked = checked.reverse();
        setChekingDates(checked);
    }, [operations]);

    useEffect(() => {
        setNameMonth(['Січня',
        'Лютого',
        'Березня',
        'Квітня',
        'Травня',
        'Червня',
        'Липня',
        'Серпня',
        'Вересня',
        'Жовтня',
        'Листопада',
        'Грудня'])
    }, [operations]);

    return (<div className="m-auto col-12 col-lg-10 text-start">
        {chekingDates.map((chekingDate, index) =>      
            <Alert key={index} variant="dark" className="my-3 px-0 px-sm-3 mx-lg-3">
                <Alert.Heading className="px-2 px-sm-3"> {chekingDate.date} {nameMonth[chekingDate.month]} {chekingDate.year}</Alert.Heading>
                <Operation chekingDate={chekingDate}></Operation>
            </Alert>
        )}        
     </div>);
}