
import { useRef, useContext, useState } from "react";
import { Form, FloatingLabel, Button } from "react-bootstrap";
import Modal from 'react-bootstrap/Modal';
import StorageContext from "../../context/StorageContext";

export default function DeletePlan({showDelete, hideDelete, loadClass}) {
    const {allPlans, setAllPlans} = useContext(StorageContext);
    const [isDisabledValue, setIsDisabledValue] = useState(false);
    const idRef = useRef(null);

    function deletePlan() {
        let buffer = allPlans.filter(el => el.id === idRef.current.value);
        let storageBuffer = allPlans.slice(0);
        storageBuffer.splice(storageBuffer.indexOf(buffer[0]), 1);
        localStorage.setItem('Plans', JSON.stringify(storageBuffer)); 
        setAllPlans(storageBuffer);
        close();
    };

    function checkForm() {
        let value = false;
        if (!idRef.current.value) {value = true; setIsDisabledValue(true)}
        if (value) {return}
        else {deletePlan()}
    }

    function close() {
        setIsDisabledValue(false);
        hideDelete();
    }

    return <><Modal show={showDelete} onHide={hideDelete} backdrop="static" centered>
        <Modal.Header>
            <Modal.Title>Видалити план</Modal.Title>
        </Modal.Header>
        <Modal.Body>
        <Form className='my-2 align-items-center'>
                <Form.Select ref={idRef} 
                    className='mb-2' 
                    aria-label="План" 
                    onChange={() => setIsDisabledValue(false)} 
                    required 
                    isInvalid={isDisabledValue}>
                <option key='heading-string' value=''>План</option>
                {allPlans.map(el => <option key={el.id} value={el.id}>{el.name}</option>)}
                </Form.Select>
                <Form.Control.Feedback type="invalid">
                    План не вибрано
                </Form.Control.Feedback>
            </Form>
            <hr />
            <div className='d-flex justify-content-between'>
                <Button variant="secondary" onClick={close}>
                    Скасувати
                </Button>
                <Button variant="primary" onClick={checkForm} disabled={isDisabledValue}>
                    Видалити
                </Button>
            </div>
        </Modal.Body>
    </Modal></>
}