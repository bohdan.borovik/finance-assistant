import { useContext, useEffect, useState } from 'react';
import StorageContext from "../../context/StorageContext";
import CreateOperationModal from "./CreateOperationModal";
import { Button, Alert } from 'react-bootstrap';
import SinglePlan from './SinglePlan';
import ButtonsBlock from './Buttons/ButtonsBlock';



export default function Plans() {

    const {allPlans, setAllPlans} = useContext(StorageContext);
    const [viewPlans, setViewPlans] = useState([]);

    useEffect(() => {
        let buffer = allPlans.slice(0);
        buffer = buffer.sort((a,b) => a.date<b.date ? -1 : 1).sort((a,b) => a.month<b.month ? -1 : 1).sort((a,b) => a.year<b.year ? -1 : 1);
        setViewPlans(buffer);       
    }, [allPlans])

    function deleteSelectedPlan() {
        let buffer = allPlans.filter(el => el.id === planID);
        let storageBuffer = allPlans.slice(0);
        if (buffer[0].repeat) {
            if (buffer[0].month === 11) {buffer[0].month = 0; buffer[0].year += 1}
            else {buffer[0].month += 1}
            buffer.forEach(el => storageBuffer.splice(storageBuffer.indexOf(el), 1, buffer[0]))
        }
        else {
        buffer.forEach(el => storageBuffer.splice(storageBuffer.indexOf(el), 1))
        }
        setAllPlans(storageBuffer);
    }

    const [showCreateOperation, setShowCreateOperation] = useState(false);
    const [planID, setPlanID] = useState(false);
  
    const hideCreateOperation = () => {setShowCreateOperation(false)};
    const openCreateOperation = (id) => {setShowCreateOperation(true); setPlanID(id)};
    const planDone = () => {deleteSelectedPlan()}

    return (<div className="container fix-main px-0 px-sm-3 py-3 mb-5 mb-lg-0 bg-secondary ">
        <div className='d-flex my-3 mx-0 mx-lg-3'>
            <div className="d-none d-lg-block col-md-2 mx-md-1 text-center">
                <ButtonsBlock />
            </div>
            <div className="px-0 px-lg-3 col-12 col-lg-10 text-center">     
                <Alert variant="primary" className='w-100'>
                    <Alert.Heading className='text-end'>Планування</Alert.Heading>
                    <hr />
                    <div className="row">
                    {viewPlans.map((plan, indx) => <SinglePlan plan={plan} indx={indx} openCreateOperation={openCreateOperation}/>)}
                    </div>
                    <hr />
                    <div className="d-flex d-lg-none justify-content-between">
                        <ButtonsBlock />
                    </div>
                </Alert>                
            </div>
        </div>
        <CreateOperationModal showCreate={showCreateOperation} hideCreate={hideCreateOperation} classes={['funds', 'outlays']} plan={true} planDone={planDone}/>
    </div>)
    ;
}