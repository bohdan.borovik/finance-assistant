import { useEffect, useState } from 'react';
import Card from 'react-bootstrap/Card';
import { SvgIcon } from '@mui/material';
import Replay30Icon from '@mui/icons-material/Replay30';

export default function SinglePlan({plan, indx, openCreateOperation}) {

    const [red, setRed] = useState(0);
    const [green, setGreen] = useState(0);

    useEffect(() => {
        let now = new Date();
        let data = new Date(`${plan.year}-${plan.month+1}-${plan.date}`)
        let daysLag = Math.ceil((data.getTime() - now.getTime()) / (1000 * 3600 * 24));

        if (daysLag > 30) {setRed(0); setGreen(255)}
        if (daysLag <= 30 && daysLag > 15) {
            let colorGreen = 255;
            let colorRed = (30 - daysLag)*17;
            setRed(colorRed); setGreen(colorGreen);
        }
        if (daysLag <= 15) {
            let colorGreen = (daysLag)*17;
            let colorRed = 255;
            setRed(colorRed); setGreen(colorGreen);
        }
    }, [plan])

    return <>
    <div key={indx} className="col-12 col-sm-6 col-md-4 col-xl-3 p-2">
    <Card key={plan.name} 
        className="h-100 text-dark" 
        style={{background: `rgba(${red},${green},0,0.4)`}}
        onClick={() => openCreateOperation(plan.id)}>
        <Card.Body className='d-flex flex-column justify-content-between' >
            <div>
                <Card.Title className="text-muted">{plan.date}-{plan.month+1}-{plan.year}</Card.Title>
                <hr />
                <Card.Subtitle className="mb-2">{plan.name}</Card.Subtitle>
                <Card.Text className="text-start">{plan.description}</Card.Text>
            </div>
            <div >
                <hr />
                {plan.repeat ? <div className='d-flex justify-content-between'>
                    <SvgIcon className='align-text-bottom' component={Replay30Icon} style={{fontSize: "2rem", marginTop: "-.5rem", marginBottom: "-1rem"}}/>
                    <Card.Subtitle className="text-end">{plan.value} UAH</Card.Subtitle>
                </div> : <Card.Subtitle className="text-end">{plan.value} UAH</Card.Subtitle>}

            </div>            
        </Card.Body>
    </Card></div></>;
}