import { useContext, useEffect, useState } from "react";
import OperationsContext from "../../context/OperationsContext";

import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import StorageContext from "../../context/StorageContext";
import { Alert } from "react-bootstrap";

export default function SingleOperation() {
    const {operations, idMsg, setIdMsg, start, setStart, end, setEnd} = useContext(OperationsContext);
    const {storage, setStorage, fundsCategories, setFundsCategories} = useContext(StorageContext);

    const [show, setShow] = useState(false);

    const handleClose = () => {setShow(false)};


    const [showOperation, setShowOperation] = useState([]);

    function deleteOperation() {
      let buffer = storage.slice(0);
      buffer.splice(storage.indexOf(showOperation), 1);
      localStorage.setItem('Storage', JSON.stringify(buffer));
      setStorage(buffer);
      let categoriesBuffer = [];
      if (showOperation.class === 'funds') {categoriesBuffer = fundsCategories.map(el => ({...el, value: el.category === showOperation.category ? el.value - showOperation.value : el.value}));}
      else {categoriesBuffer = fundsCategories.map(el => ({...el, value: el.category === showOperation.fund ? el.value + showOperation.value : el.value}));}
      setFundsCategories(categoriesBuffer);
      setStart(Object.assign({}, start));
      setEnd(Object.assign({}, end));
      handleClose();
    }

    useEffect(() => {
      if (idMsg) {
          operations.filter(operation => operation.id === idMsg).map(operation => setShowOperation(operation));
          setShow(true);            
          setIdMsg(0);
      }
    }, [idMsg])

    return (
        <Modal show={show} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>{`${showOperation.date}-${showOperation.month + 1}-${showOperation.year}`}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="p-0">
          <Alert variant={showOperation.class === 'funds' ? "success" : "danger"} className="m-0">
            <h4 className="text-center">{showOperation.class === 'funds' ? 'Дохід' : 'Витрата'}</h4>
            <p><h6>Категорія: {showOperation.category}</h6></p>
            {showOperation.class !== 'funds' ? <p><h6>З рахунку: {showOperation.fund}</h6></p> : ''}
            {showOperation.subcategory ? <p><h6>Коментар: {showOperation.subcategory}</h6></p> : ''}
            {showOperation.description !== 'funds' ? <p><h6>Опис: {showOperation.description}</h6></p> : ''}
            <p><h6>Сума: {showOperation.value}</h6></p>
          </Alert> 
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={deleteOperation}>
            Видалити
          </Button>
          <Button variant="primary" onClick={handleClose}>
            Редагувати
          </Button>
        </Modal.Footer>
      </Modal>
  );
}