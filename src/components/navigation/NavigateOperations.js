import { useEffect, useState } from 'react';
import { ToggleButton } from 'react-bootstrap';
import Nav from 'react-bootstrap/Nav';
import { NavLink, useLocation } from 'react-router-dom';
import { SvgIcon } from '@mui/material';
import CreditCardIcon from '@mui/icons-material/CreditCard';
import ReceiptIcon from '@mui/icons-material/Receipt';
import DataSaverOffIcon from '@mui/icons-material/DataSaverOff';
import FactCheckIcon from '@mui/icons-material/FactCheck';
import './navigateOperations.css'

export default function NavigateOperations() {

    const [activeElement, setActiveElement] = useState('0');

    const buttons = [
        { name: 'Рахунки', value: '1', path: 'funds', icon: CreditCardIcon},
        { name: 'Операції', value: '2', path: 'operations', icon: ReceiptIcon},
        { name: 'Категорії', value: '3', path: 'categories', icon: DataSaverOffIcon},
        { name: 'Планування', value: '4', path: 'plans', icon: FactCheckIcon}
    ];

    const location = useLocation();

    useEffect(() => {
      let path = location.pathname;
      path = path.slice(1,path.length);
      buttons.forEach(button => button.path === path ? setActiveElement(button.value) : '')
    }, [])

  return (
    <>
    <Nav fill variant="tabs" className='d-none d-sm-flex container bg-secondary'>
      {buttons.map((el, indx) => (<Nav.Item key={indx}>
        <NavLink key={indx} to={el.path} className={'link'}>
            <ToggleButton
                key={indx}                
                className="my-2 navigate-btn"
                id={`toggle-check-${indx}`}
                type="radio"
                variant="outline-light"
                checked={activeElement === el.value}
                value={el.value}
                onClick={() => setActiveElement(el.value)}>
                {el.name}
            </ToggleButton>
        </NavLink>
      </Nav.Item>))}
    </Nav>
    <Nav fill variant="tabs" className='d-flex d-sm-none position-fixed bottom-0 container bg-dark nav-bar-xs'>
      {buttons.map((el, indx) => (<Nav.Item key={indx}>
        <NavLink key={indx} to={el.path} className={'link'}>
            <ToggleButton
                key={indx}                
                className="my-2 navigate-btn-rounded"
                id={`toggle-check-${indx}`}
                type="radio"
                variant="outline-light"
                checked={activeElement === el.value}
                value={el.value}
                onClick={() => setActiveElement(el.value)}>
                <SvgIcon component={el.icon} style={{fontSize: "2rem"}}/>
            </ToggleButton>
        </NavLink>
      </Nav.Item>))}
    </Nav>
    </>
  );
}