import { useEffect, useContext } from "react";
import OperationsContext from "../../context/OperationsContext";
import ProfileContext from "../../context/ProfileContext";
import StorageContext from "../../context/StorageContext";

export default function Storage() {
    
    const {user} = useContext(ProfileContext);
    const {storage, setStorage, setFundsCategories, setOutlaysCategories, setAllPlans} = useContext(StorageContext);
    const {setOperations} = useContext(OperationsContext);    

    useEffect(() => { 
        if (!user.isLogIn) {return ;}
        try {
            let data = JSON.parse(localStorage.getItem('Storage'));
            if (!data) {
                throw new Error();
            }
            setStorage(data);
        }
        catch (e) {
            setStorage([
                {id: 1,
                    class: 'funds',
                    icon: '', 
                    category: 'Готівка', 
                    subcategory: 'Заробітна плата', 
                    value: 25000,
                    date: 2,
                    month: 8,
                    year: 2022,
                    description: '',
                    },
                {id: 2,
                    class: 'funds',
                    icon: '', 
                    category: 'Картка', 
                    subcategory: 'Фріланс', 
                    value: 4500,
                    date: 10,
                    month: 8,
                    year: 2022,
                    description: 'Оплата замовлення',
                    },
                {id: 3,
                    class: 'outlays',
                    icon: '', 
                    category: 'Продукти', 
                    fund: 'Готівка',
                    subcategory: '', 
                    value: 1800,
                    date: 11,
                    month: 8,
                    year: 2022,
                    description: 'Закупка на тиждень',
                    },
                {id: 4,
                    class: 'outlays',
                    icon: '', 
                    category: 'Розваги', 
                    fund: 'Картка', 
                    subcategory: 'Прогулянки',
                    value: 150,
                    date: 11,
                    month: 8,
                    year: 2022,
                    description: 'Кава в парку',
                    },
                {id: 5,
                    class: 'outlays',
                    icon: '', 
                    category: 'Одяг і взуття', 
                    fund: 'Готівка', 
                    subcategory: 'Взуття', 
                    value: 3000,
                    date: 15,
                    month: 8,
                    year: 2022,
                    description: 'Пара нових кросівок',
                    },
                {id: 6,
                    class: 'funds',
                    icon: '', 
                    category: 'Картка', 
                    subcategory: 'Фріланс', 
                    value: 2000,
                    date: 15,
                    month: 8,
                    year: 2022,
                    description: 'Рендер моделі',
                    },
                {id: 7,
                    class: 'outlays',
                    icon: '', 
                    category: 'Дім', 
                    fund: 'Готівка', 
                    subcategory: '', 
                    value: 750, 
                    date: 18,
                    month: 8,
                    year: 2022,
                    description: 'Заправка авто',
                    },
                {id: 8,
                    class: 'funds',
                    icon: '', 
                    category: 'Картка', 
                    subcategory: 'Фріланс', 
                    value: 1500,
                    date: 3,
                    month: 9,
                    year: 2022,
                    description: 'Оплата за 3Д модель',
                    }
                
            ]);
        } 
        
        try {
            let data = JSON.parse(localStorage.getItem('FundsCategories'));
            if (!data) {
                throw new Error();
            }
            setFundsCategories(data);
        }
        catch (e) {
            let data = [
                {id: 1,
                    icon: '', 
                    category: 'Готівка', 
                    value: 19450
                },
                {id: 2,
                    icon: '', 
                    category: 'Картка', 
                    value: 7850
                },
                {id: 3,
                    icon: '', 
                    category: 'Подарунки', 
                    value: 0
                }              
            ];

            localStorage.setItem('FundsCategories', JSON.stringify(data));
            setFundsCategories(data);
        }

        try {
            let outlay_data = JSON.parse(localStorage.getItem('OutlaysCategories'));
            if (!outlay_data) {
                throw new Error();
            }
            setOutlaysCategories(outlay_data);
        }
        catch (e) {
            let outlay_data = [
                {id: 1,
                    icon: '', 
                    category: 'Продукти'
                },
                {id: 2,
                    icon: '', 
                    category: 'Дім'
                },
                {id: 3,
                    icon: '', 
                    category: 'Одяг і взуття'
                },
                {id: 4,
                    icon: '', 
                    category: 'Розваги'
                }            
            ];

            localStorage.setItem('OutlaysCategories', JSON.stringify(outlay_data));
            setOutlaysCategories(outlay_data);
        }

        try {
            let plans_data = JSON.parse(localStorage.getItem('Plans'));
            if (!plans_data) {
                throw new Error();
            }
            setAllPlans(plans_data);
        }
        catch (e) {
            let plans_data = [
                {id: 1,
                    name: 'Дім',
                    description: 'Оплата комунальних послуг',
                    date: 20,
                    month: 9,
                    year: 2022,
                    repeat: true,
                    value: 1000
                    },
                {id: 2,
                    name: 'Розстрочка',
                    description: 'Оплата розстрочки за телевізор',
                    date: 28,
                    month: 9,
                    year: 2022,
                    repeat: true,
                    value: 450
                    },
                {id: 3,
                    name: 'День народження сестри',
                    description: 'Купити подарунок на день народження',
                    date: 11,
                    month: 10,
                    year: 2022,
                    repeat: false,
                    value: 2000
                    },
                {id: 4,
                    name: 'Світильник',
                    description: 'Купити світильник в спальню',
                    date: 9,
                    month: 9,
                    year: 2022,
                    repeat: false,
                    value: 500
                    }         
            ];

            localStorage.setItem('Plans', JSON.stringify(plans_data));
            setAllPlans(plans_data);
        }
        
    }, []);

    useEffect(() => {setOperations(storage)}, [storage]);
}