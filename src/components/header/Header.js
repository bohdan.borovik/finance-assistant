import { Container } from "react-bootstrap";
import { useState, useContext, useEffect } from 'react';
import ProfileContext from '../../context/ProfileContext';
import User from "./User";
import './header.css'
import { SvgIcon } from "@mui/material";
import AccountCircleIcon from '@mui/icons-material/AccountCircle';
import UserMenu from "./UserMenu";

function Header() {
    const {user} = useContext(ProfileContext);
    const [localeUser, setLocaleUser] = useState({})

    useEffect(() => {
        setLocaleUser(user);
    }, [user])

    const [show, setShow] = useState(false)

    const handleOpen = () => setShow(true);

    const handleClose = () => setShow(false);

    return (<>
        <div className="bg-dark py-3">
            <Container className="d-flex justify-content-between head">
                <div className="align-self-center text-white">Фінансовий помічник</div>
                <div className="align-self-center text-white" onClick={handleOpen}>
                    {localeUser.isLogIn === true ? 
                    <>
                    <User localeUser={localeUser}/> 
                    <SvgIcon component={AccountCircleIcon} className="mb-1" style={{fontSize: "2rem"}}/>
                    </>
                    : ''}
                </div>
            </Container>
        </div>
        {localeUser.isLogIn === true ? <UserMenu show={show} handleClose={handleClose}/>: ''}</>
    );
}

export default Header;