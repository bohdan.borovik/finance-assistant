import { useContext } from "react";
import { Button, Offcanvas } from "react-bootstrap";
import ProfileContext from "../../context/ProfileContext";

export default function UserMenu({show, handleClose}) {
    const {user, setUser} = useContext(ProfileContext);

    function exit() {
        let buffer = Object.assign({}, user)
        buffer.isLogIn = false;
        localStorage.setItem('Profile',JSON.stringify(buffer));
        setUser(buffer);
        handleClose();
    }

    return <Offcanvas show={show} onHide={handleClose} placement="end">
                <Offcanvas.Header closeButton>
                    <Offcanvas.Title>{user.login}</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                    <Button onClick={exit} variant='dark'>Вийти з профілю</Button>
                </Offcanvas.Body>
    </Offcanvas>;
}