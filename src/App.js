
import 'bootstrap/dist/css/bootstrap.min.css';
import { useState, useEffect } from 'react';
import Header from './components/header/Header';
import Operations from './components/main/Operations';
import ProfileContext from './context/ProfileContext'
import OperationsContext from './context/OperationsContext';
import Storage from './components/storage/Storage';

import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import StorageContext from './context/StorageContext';
import NavigateOperations from './components/navigation/NavigateOperations';
import Funds from './components/main/Funds';
import Categories from './components/main/Categories';
import Plans from './components/main/Plans';
import Start from './components/main/Start';
import NotLogIn from './components/main/NotLogIn';
import NotFound from './components/main/NotFound';

function App() {
  const [user, setUser] = useState({login:'', password:'', isLogIn: false});
  const [calc, setCalc] = useState(0);

  const [operations, setOperations] = useState([]);
  const [start, setStart] = useState([]);
  const [end, setEnd] = useState([]);
  const [idMsg, setIdMsg] = useState(0);

  const [storage, setStorage] = useState([]);
  const [fundsCategories, setFundsCategories] = useState([]);
  const [outlaysCategories, setOutlaysCategories] = useState([]);
  const [allPlans, setAllPlans] = useState([]);

  useEffect(() => {
    let storageData = JSON.parse(localStorage.getItem('Profile'));
    if (storageData) {setUser({login: storageData.login, password: storageData.password, isLogIn: storageData.isLogIn})}; 
  } , []);

  return (
    <ProfileContext.Provider value={{user, setUser, calc, setCalc}}>
      <StorageContext.Provider value={{storage, setStorage, fundsCategories, setFundsCategories, outlaysCategories, setOutlaysCategories, allPlans, setAllPlans}}>
      <OperationsContext.Provider value={{operations, setOperations, idMsg, setIdMsg, start, setStart, end, setEnd}}>
        <BrowserRouter>
          <Header />
          {user.isLogIn === false ? 
          <Routes>          
            <Route index element={<Start />} />   
            <Route path="*" element={<NotLogIn />} />
          </Routes> :
          <>
          <Storage />
          <NavigateOperations />
          <Routes>          
            <Route index element={<Operations />} />            
            <Route path="operations" element={<Operations />} />
            <Route path="funds" element={<Funds />} />
            <Route path="categories" element={<Categories />} />
            <Route path="plans" element={<Plans />} />
            <Route path="*" element={<NotFound />} />            
          </Routes></>}
        </BrowserRouter>
      </OperationsContext.Provider>
      </StorageContext.Provider>
    </ProfileContext.Provider>
  );
}

export default App;